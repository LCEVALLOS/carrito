package com.example.carrito.Util;

import com.example.carrito.Model.Pro;
import com.example.carrito.Model.Productos;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ProductoServices {

     @GET("categoria-listar-productos/?VHozaS85TU9uUnhTR2FpMWh0eUJCZz09=gAAAAABgAGpunQZzKslbNqIL71S6nhjanaqWYmni6w7Bv_i0nc49t4WyDc3X6fPWVYzx2Lg_3b8PabFJ5RUF2rS43OGWXQ-Yuw%3D%3D&id_sucursal=20&id_categoria=485&id_subcategoria=0&offset=0&limit=100")
    Call <Pro> getProducto();
}
