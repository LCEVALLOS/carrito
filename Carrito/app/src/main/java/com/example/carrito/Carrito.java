package com.example.carrito;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carrito.Model.CarritoModel;
import com.example.carrito.adaptadores.CarritoAdapter;
import com.example.carrito.adaptadores.ProductoAdpater;

import java.util.ArrayList;
import java.util.List;

public class Carrito extends AppCompatActivity {

    List<CarritoModel> productos = new ArrayList<>();
    ListView lsCarrito;
    TextView txtTotal;
    double total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);
        productos = ProductoAdpater.addcar;
        txtTotal = (TextView)findViewById(R.id.txtTotal);


        if(login.user==0){
            Intent intent = new Intent(this, login.class);
            this.startActivity(intent);

        }



        lsCarrito= (ListView)findViewById(R.id.lstCarrito);


        getCarrito();

    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return  true;
    }
    public  boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.mHome:
                Intent intent= new Intent(this,MainActivity.class);
                this.startActivity(intent);
                return true;

            case R.id.mSalir:
                Intent principal= new Intent(this,MainActivity.class);
                login.user=0;
                this.startActivity(principal);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void getCarrito(){

        //VAlidacacion para saber si el objeto carrito esta lleno
        if(productos.size()>0 || productos!=null){
            //Calculando el total del carrito
            for (CarritoModel p:productos) {

                total= total+(p.getCantidad() * Double.parseDouble(p.getPrecio()) );

            }
            txtTotal.setText("$"+String.valueOf(total));

            //Seteando el objeto carrito al adaptador que se usara en el listview
            lsCarrito.setAdapter(new CarritoAdapter(Carrito.this,R.layout.list_car,productos));
        }
        else {
            Toast.makeText(this, "Vacio", Toast.LENGTH_SHORT).show();
        }

    }
    public  void comprar(View vie){

        if(productos.size() > 0 && productos!=null){

            ProductoAdpater.addcar.clear();
            productos.clear();
            Intent intent = new Intent(this, MainActivity.class);

            this.startActivity(intent);
            Toast.makeText(this, "Gracias por su compra", Toast.LENGTH_SHORT).show();
        }

        else {
            Toast.makeText(this, "El carrito se encuentra vacio", Toast.LENGTH_SHORT).show();
        }


    }

}