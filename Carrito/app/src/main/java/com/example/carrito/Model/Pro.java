package com.example.carrito.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Pro {



   @SerializedName("productos")
    @Expose
    private ArrayList<Productos>productos;

     public ArrayList<Productos> getProductos() {
        return productos;
    }

   public void setProductos(ArrayList<Productos> productos) {
        this.productos = productos;
    }

}
