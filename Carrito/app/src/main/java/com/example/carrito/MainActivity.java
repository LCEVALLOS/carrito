package com.example.carrito;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.carrito.Model.Pro;
import com.example.carrito.Model.Productos;
import com.example.carrito.Util.ProductoServices;
import com.example.carrito.adaptadores.ProductoAdpater;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    ProductoServices productoServices;
    List<Productos> listaProductos = new ArrayList<>();
    Productos p;
    ListView lsHome;

    String usuario="Luis";
    private static  final String TAG ="Producto";
    private final  static  String url ="https://gestion.promo.ec/promo/productos/ws/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

         setContentView(R.layout.activity_main);
        lsHome = (ListView)findViewById(R.id.lvHome);
        Api();

    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return  true;
    }
    public  boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.mCarrito:
                Intent intent= new Intent(this,Carrito.class);
                    this.startActivity(intent);
                    return true;

            case R.id.mSalir:
                Intent principal= new Intent(this,MainActivity.class);
                login.user=0;
                this.startActivity(principal);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public  void Api(){
        //Uso de la clase rettrofit para cosumir el servicio

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gestion.promo.ec/promo/productos/ws/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProductoServices productoServices= retrofit.create(ProductoServices.class);
        Call <Pro>lista2= productoServices.getProducto();


        lista2.enqueue(new Callback<Pro>() {
            @Override
            public void onResponse(Call<Pro> call, Response<Pro> response) {
                //Validaciion para saber si la comunicacion fue exitosa
                if(response.isSuccessful()){

                    Pro pro = response.body();
                    ArrayList<Productos> productos= pro.getProductos();

                    //Seteando  los datos del api al adaptor del layout que mostrara los productos
                    lsHome.setAdapter(new ProductoAdpater(MainActivity.this,R.layout.list_home,productos,usuario));

                }
                else {
                    Log.e(TAG, "Error "+ response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<Pro> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }

}