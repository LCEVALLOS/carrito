package com.example.carrito.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarritoModel {


    private Integer id_producto;


    private Integer tipo;


    private Integer cod_principal;


    private String nombre;


    private String descripcion;


    private String precio;


    private Integer cantidad;


    private String imagenes;

    public CarritoModel(Integer id_producto, Integer tipo, Integer cod_principal, String nombre, String descripcion, String precio, Integer cantidad, String imagenes) {
        this.id_producto = id_producto;
        this.tipo = tipo;
        this.cod_principal = cod_principal;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad = cantidad;
        this.imagenes = imagenes;
    }

    public Integer getId_producto() {
        return id_producto;
    }

    public void setId_producto(Integer id_producto) {
        this.id_producto = id_producto;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getCod_principal() {
        return cod_principal;
    }

    public void setCod_principal(Integer cod_principal) {
        this.cod_principal = cod_principal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getImagenes() {
        return imagenes;
    }

    public void setImagenes(String imagenes) {
        this.imagenes = imagenes;
    }

    @Override
    public String toString() {
        return "CarritoModel{" +
                "id_producto=" + id_producto +
                ", tipo=" + tipo +
                ", cod_principal=" + cod_principal +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", precio='" + precio + '\'' +
                ", cantidad=" + cantidad +
                ", imagenes='" + imagenes + '\'' +
                '}';
    }
}

