package com.example.carrito.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.carrito.Model.CarritoModel;
import com.example.carrito.R;

import java.util.List;

public class CarritoAdapter extends ArrayAdapter<CarritoModel> {
    private Context context;
    private List<CarritoModel> productos;
    private int layout;

    public CarritoAdapter(@NonNull Context context, int resource, @NonNull List<CarritoModel> objects) {
        super(context, resource, objects);
        this.context=context;
        this.productos=objects;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.list_car,parent,false);
        //Creando comunicacion con los compontes deel layout
        String url=productos.get(position).getImagenes();
        TextView txtNombre =(TextView)view.findViewById(R.id.txtCNombre);
        TextView txtPrecio =(TextView)view.findViewById(R.id.txtCPrecio);
        TextView txtCantidad =(TextView)view.findViewById(R.id.txtCcantidad);
        ImageView imgProducto=(ImageView)view.findViewById(R.id.imgCproducto);

        // Seteando los valores a los compontes del layout
        txtNombre.setText(String.format("%s",productos.get(position).getNombre()));
        txtPrecio.setText(String.format("$%s",productos.get(position).getPrecio()));
        txtCantidad.setText(String.format("%s",productos.get(position).getCantidad()));
        Glide.with(context).load(url).into(imgProducto);

        return view;
    }
}
