package com.example.carrito.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.carrito.Carrito;
import com.example.carrito.Detalle;
import com.example.carrito.Model.CarritoModel;
import com.example.carrito.Model.Productos;
import com.example.carrito.R;
import com.example.carrito.login;

import java.util.ArrayList;
import java.util.List;

public class ProductoAdpater extends ArrayAdapter<Productos> {
    private  Context context;
    private  List<Productos>productos;
    private  List<CarritoModel>carProd  = new ArrayList<>();;
    private int layout;
    private  String usuario;
    ListView lsCarrito;
    private static  final String TAG ="Producto";

    boolean existe=false;

    public static List<CarritoModel> addcar = new ArrayList<>();
     public ProductoAdpater(@NonNull Context context, int resource, @NonNull List<Productos> objects,String user) {
        super(context, resource, objects);
        this.context=context;
        this.productos=objects;
        this.usuario=user;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       LayoutInflater layoutInflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView= layoutInflater.inflate(R.layout.list_home,parent,false);
         String url="https://gestion.promo.ec"+productos.get(position).getImagenes().get(0);

        //Creando la comunicacion con los componentes del layout
        ImageButton btFavorito = (ImageButton) rowView.findViewById(R.id.imageButton);
        TextView txtNombre =(TextView)rowView.findViewById(R.id.txtnombre);
        TextView txtPrecio =(TextView)rowView.findViewById(R.id.txtDprecio);
        ImageView imgProducto=(ImageView)rowView.findViewById(R.id.imageView);

        //Setear datos a los compontes que estan en la layout
        txtNombre.setText(String.format("%s",productos.get(position).getNombre()));
        txtPrecio.setText(String.format("%s",productos.get(position).getPrecio()));
        Glide.with(context).load(url).into(imgProducto);

          rowView.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  //Pasando los valores para el detalle del producto
                  Intent intent = new Intent(context, Detalle.class);
                  String nombre = txtNombre.getText().toString();
                  String precio = txtPrecio.getText().toString();
                  intent.putExtra("nombre", nombre);
                  intent.putExtra("precio", precio);
                  intent.putExtra("imagen", url);
                  intent.putExtra("usuario",usuario);
                  intent.putExtra("id_producto",productos.get(position).getId_producto());

                 context.startActivity(intent);

              }
          });

        btFavorito.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Validacion para saber si hya un usuario logeado al momento de agregar un producto al carrito
                if(login.user!=0){

                    int id = productos.get(position).getId_producto();
                    //Reccorrer el objeto carrito para buscar el producto
                    for (CarritoModel p:addcar) {
                        int idcarrito=p.getId_producto();
                        existe=false;
                        //Validacion para saber si el producto que se agrego al carrito ya existe si es asi se le sumara la cantidad
                        if ( idcarrito == id ) {
                            p.setCantidad(p.getCantidad()+1);
                            existe=true;
                            break;
                        }
                    }
                    // Si el producto no existe en el carrito se lo agregara con una cantidad de 1
                    if(existe ==false){

                        CarritoModel producto= new CarritoModel(
                                productos.get(position).getId_producto(),
                                productos.get(position).getTipo(),
                                productos.get(position).getCod_principal(),
                                productos.get(position).getNombre(),
                                productos.get(position).getDescripcion(),
                                productos.get(position).getPrecio(),
                                1,
                                url
                        );
                        addcar.add(producto);

                    }

                    if(addcar.size()<1){
                        Toast.makeText(context, "No se guardo el producto", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(context, "Se agrego al carrito el producto "+ txtNombre.getText().toString(), Toast.LENGTH_SHORT).show();

                    }

                }
                //Si no hayn un usuariom logeado al momento de agregar el producto al carrito sera redireccionadop al login
                else {
                    Intent intent = new Intent(context,login.class);
                    context.startActivity(intent);
                }


            }

        });

        return  rowView;

    }
}
