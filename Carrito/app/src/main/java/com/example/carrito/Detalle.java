package com.example.carrito;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ComponentActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.carrito.Model.CarritoModel;
import com.example.carrito.adaptadores.ProductoAdpater;

public class Detalle extends AppCompatActivity {
    TextView txtNombre, txtPrecio;
    ImageView imagen;
    String usuario;
    int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        txtNombre = (TextView)findViewById(R.id.txtDmombre);
        txtPrecio = (TextView) findViewById(R.id.txtDprecio);
        imagen =(ImageView)findViewById(R.id.imgDetalle);

        //Recuperando los datos que se usaran en los componentes del detalle
        Bundle bundle = getIntent().getExtras();

        String nombre = bundle.getString("nombre");
        String precio = bundle.getString("precio");
        String imagenes = bundle.getString("imagen");
        id= bundle.getInt("id_producto");
        usuario = bundle.getString("usuario");
        if(usuario!=null){
            Toast.makeText(this, "Hola "+usuario, Toast.LENGTH_SHORT).show();
        }
        txtNombre.setText(nombre);
        txtPrecio.setText(""+precio);
        Glide.with(this).load(imagenes).into(imagen);


    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return  true;
    }
    public  boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){

            case R.id.mHome:
                Intent home= new Intent(this,MainActivity.class);

                this.startActivity(home);
                return true;

            case R.id.mCarrito:
                Intent intent= new Intent(this,Carrito.class);
                this.startActivity(intent);
                return true;

            case R.id.mSalir:
                Intent logout= new Intent(this,MainActivity.class);
                login.user=0;
                this.startActivity(logout);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public  void  Carrito (View view){
        Intent intent = new Intent(this, Carrito.class);
        this.startActivity(intent);
    }

}